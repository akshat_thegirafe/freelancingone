// SCROLL TO TOP JS STARTS
  $(document).ready(function(){ 
    $(window).scroll(function(){ 
      if ($(this).scrollTop() > 100) { 
        $('#scroll').fadeIn(); 
      } 
      else{ 
        $('#scroll').fadeOut(); 
      } 
    }); 
    $('#scroll').click(function(){ 
    $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    });   
  });
// SCROLL TO TOP JS ENDS


// HEADER FOOTER CALL FUNCTION STARTS
  $(function(){
    $("#header").load("header.html");
    $("#footer").load("footer.html");
  });
// HEADER FOOTER CALL FUNCTION ENDS



// STICKY JS STARTS
  // window.onscroll = function() {myFunction()};
  // var navbar = document.getElementById("myNavbar");
  // var sticky = navbar.offsetTop;
  // function myFunction() {
  //   if (window.pageYOffset >= sticky) {
  //     navbar.classList.add("sticky")
  //   } 
  //   else{ 
  //     navbar.classList.remove("sticky");
  //   }
  // }

  $(window).on('scroll', function() {
    if ($(window).scrollTop() > 200) {
      $('#myNavbar').addClass('sticky');
    } else {
      $('#myNavbar').removeClass('sticky');
    }
  });
// STICKY JS ENDS


// ENCRYPTION STARTS
  // document.onkeydown = function(e) {
  //   if(e.keyCode == 123) {
  //     return false;
  //   }
  //   if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
  //     return false;
  //   }
  //   if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
  //     return false;
  //   }
  //   if(e.ctrlKey && e.shiftKey && e.keyCode == 'M'.charCodeAt(0)){
  //     return false;
  //   }
  //   if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
  //     return false;
  //   }
  //   if(e.ctrlKey && e.keyCode == 'C'.charCodeAt(0)){
  //     return false;
  //   }
  //   if(e.ctrlKey && e.keyCode == 'X'.charCodeAt(0)){
  //     return false;
  //   }
  //   if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)){
  //     return false;
  //   }      
  // }

  // $(document).ready(function() {
  //   $("body").on("contextmenu",function(){
  //     return false;
  //   }); 
  // });

  // document.addEventListener('contextmenu', event => event.preventDefault());
  // $("img").mousedown(function(){
  //   return false;
  // });
// ENCRYPTION ENDS